package ga.expeditenetwork.listeners;

import ga.expeditenetwork.data.Users;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.Date;
import java.util.TimeZone;

public class PlayerQuit implements Listener {

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        Users users = Users.get(player);

        event.setQuitMessage(null);

        TimeZone.setDefault(TimeZone.getTimeZone("EST"));
        Date date = new Date();

        users.getFile().set("lastlogout", date.getTime());
        users.saveFile();
    }
}
