package ga.expeditenetwork.listeners;

import ga.expeditenetwork.Core;
import ga.expeditenetwork.data.Users;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.io.File;

public class PlayerJoin implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        Users user = Users.get(player);
        Core plugin = Core.getInstance();

        event.setJoinMessage(null);
        if(user.isNew()) {
            File localFile = new File(plugin.getDataFolder() + File.separator + "users" + File.separator);
        }
        user.getFile().set("username", player.getName());
        user.getFile().set("uuid", player.getUniqueId().toString());
        user.getFile().set("ip", player.getAddress().getAddress().getHostAddress());
        user.getFile().set("lastlogin", Long.valueOf(System.currentTimeMillis()));
        user.saveFile();

        plugin.getScoreboards().refreshScoreboard(player);
    }
}
