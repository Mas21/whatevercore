package ga.expeditenetwork.listeners;

import ga.expeditenetwork.Core;
import ga.expeditenetwork.utils.DateUtils;
import org.bukkit.BanEntry;
import org.bukkit.BanList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

public class PlayerLogin implements Listener {

    @EventHandler
    public void onLogin(PlayerLoginEvent event) {
        Player player = event.getPlayer();
        Core plugin = Core.getInstance();

        if(event.getResult() == PlayerLoginEvent.Result.KICK_BANNED) {
            BanList name = Bukkit.getBanList(BanList.Type.NAME);
            BanList ip = Bukkit.getBanList(BanList.Type.IP);

            String address = event.getAddress().getHostAddress();

            if(name.getBanEntry(player.getName()) != null) {
                if(player.hasPermission("expedite.bypass")) {
                    plugin.getMessage().staffMessage(ChatColor.RED + player.getName() + " &7has bypassed a suspension.");
                    name.pardon(player.getName());
                    event.allow();
                    return;
                }
                BanEntry ban = name.getBanEntry(player.getName());

                event.setKickMessage(ChatColor.translateAlternateColorCodes('&', "&cYou have been &4" +
                        (ban.getExpiration() == null ? "suspended" : "temp-suspended") + " &cfrom the &5Expedite &fNetwork" +
                        "\n" +
                        "\n&7Reason&8: &f" + ban.getReason() +
                        "\n&7Banned by&8: &6" + ban.getSource() +
                        "\n&7Expires in&8: &c" + DateUtils.formatDateDiff(ban.getExpiration().getTime()) +
                        "\n" +
                        "\n&cIf you would like to appeal, visit our website at &7&o&nwww.(websitenamehere).com"));
            } else if(ip.getBanEntry(address) != null) {
                if(player.hasPermission("expedite.bypass")) {
                    plugin.getMessage().staffMessage(ChatColor.RED + player.getName() + " &7has bypassed an ip-suspension.");
                    name.pardon(player.getName());
                    event.allow();
                    return;
                }
                BanEntry ban = name.getBanEntry(player.getName());

                event.setKickMessage(ChatColor.translateAlternateColorCodes('&', "&cYour IP has been &4suspended &cfrom the &5Expedite &fNetwork" +
                        "\n" +
                        "\n&7Reason&8: &f" + ban.getReason() +
                        "\n&7Banned by&8: &6" + ban.getSource() +
                        "\n" +
                        "\n&cYou &4&o&lMAY NOT &cappeal an ip-suspension on the forums."));
            }
        } else if(event.getResult() == PlayerLoginEvent.Result.KICK_WHITELIST && player.hasPermission("expedite.bypass")) {
            event.allow();
        } else if(event.getResult() == PlayerLoginEvent.Result.KICK_FULL && player.hasPermission("expedite.user.bypass") || player.hasPermission("expedite.bypass")) {
            event.allow();
        }
    }
}
