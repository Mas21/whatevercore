package ga.expeditenetwork.listeners;

import ga.expeditenetwork.Core;
import ga.expeditenetwork.data.Ranks;
import ga.expeditenetwork.data.Server;
import ga.expeditenetwork.data.Users;
import ga.expeditenetwork.utils.DateUtils;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Date;
import java.util.TimeZone;

public class PlayerChat implements Listener {

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        Server server = Server.getInstance();
        Users users = Users.get(player);
        Core plugin = Core.getInstance();

        String message = event.getMessage();
        String prefix;

        event.setCancelled(true);

        if(server.getData().getBoolean("chat.muted") && !player.hasPermission("essentials.bypass")) {
            plugin.getMessage().sendMessage(player, "&cThe chat is currently muted.");
            event.setCancelled(true);
            return;
        }
        if (plugin.SLEW_COOLDOWN.containsKey(player.getUniqueId().toString())) {
            plugin.getMessage().sendMessage(player, "&7The chat is currently in slew mode. You need to wait &c" +
                    plugin.SLEW_COOLDOWN.get(player.getUniqueId().toString()) + " &7seconds to chat.");
            event.setCancelled(true);
            return;
        }
        if(users.getFile().getBoolean("staffchat")) {
            plugin.getMessage().staffMessage(ChatColor.LIGHT_PURPLE + player.getName() + "&8: &e" + ChatColor.translateAlternateColorCodes('&', message));
            return;
        }
        if(server.getData().getBoolean("chat.slew")) {
            if(player.hasPermission("essentials.bypass") || users.getFile().getBoolean("staffchat")) {
                return;
            }
            plugin.SLEW_COOLDOWN.put(player.getUniqueId().toString(), Integer.valueOf(10));
            plugin.SLEW_TASK.put(player.getUniqueId().toString(), new BukkitRunnable() {
                public void run() {
                    plugin.SLEW_COOLDOWN.put(player.getUniqueId().toString(), plugin.SLEW_COOLDOWN.get(player.getUniqueId().toString()).intValue() - 1);
                    if (plugin.SLEW_COOLDOWN.get(player.getUniqueId().toString()).intValue() == 0) {
                        plugin.SLEW_COOLDOWN.remove(player.getUniqueId().toString());
                        plugin.SLEW_TASK.remove(player.getUniqueId().toString());
                        cancel();
                    }
                }
            });
            plugin.SLEW_TASK.get(player.getUniqueId().toString()).runTaskTimer(plugin, 0L, 20L);
        }

        if (users.isMuted()) {
            TimeZone.setDefault(TimeZone.getTimeZone("EST"));
            Date date = new Date();
            if (users.getUnmuteTime() == -1 || users.getUnmuteTime() > date.getTime()) {
                plugin.getMessage().sendMessage(player, "&cYou have been muted for: &a" + users.getMutedReason());
                if (users.getUnmuteTime() < 0) {
                    plugin.getMessage().sendMessage(player, "&cYour mute is permanent.");
                } else {
                    plugin.getMessage().sendMessage(player, "&cYour mute expires in: &6" + DateUtils.formatDateDiff(users.getUnmuteTime()));
                }
                return;
            } else {
                users.unmute();
            }
        }

        //TODO: Fix chat to be more efficient.
        if(users.getRank() == Ranks.FOUNDER) {
            prefix = "&eFounder";
        } else if(users.getRank() == Ranks.ADMINISTRATOR) {
            prefix = "&4Admin";
        } else if(users.getRank() == Ranks.DEVELOPER) {
            prefix = "&9Dev";
        } else if(users.getRank() == Ranks.MODERATOR) {
            prefix = "&2Mod";
        } else if(users.getRank() == Ranks.BUILDER) {
            prefix = "&dBuilder";
        } else {
            prefix = "&7User";
        }
    }
}
