package ga.expeditenetwork.data;

public enum Ranks {

    USER, BUILDER, TRIAL_MOD, MODERATOR, DEVELOPER, ADMINISTRATOR, FOUNDER
}
