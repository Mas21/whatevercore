package ga.expeditenetwork.data;

import ga.expeditenetwork.Core;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.Date;
import java.util.TimeZone;
import java.util.UUID;

public class Users {
    private Player player;
    private String uuid;
    private UUID plainUUID;

    private Long foodStart;
    private int invalidFoodEatebleCount = 0;

    private FileConfiguration config;
    private File file;

    private boolean creating = false;

    Core plugin = Core.getInstance();

    public static Users get(Player player) {
        return new Users(player, player.getUniqueId().toString());
    }

    public static Users get(OfflinePlayer offline) {
        return new Users(offline.getPlayer(), offline.getUniqueId().toString());
    }

    private Users(Player player, String uuid) {
        if (!plugin.getDataFolder().exists()) {
            plugin.getDataFolder().mkdir();
        }

        File folder = new File(plugin.getDataFolder() + File.separator + "users" + File.separator);

        if (!folder.exists()) {
            folder.mkdir();
        }

        file = new File(folder, uuid + ".yml");

        if (!file.exists()) {
            try {
                file.createNewFile();
                creating = true;
            } catch (Exception e) {
                plugin.getLogger().severe(ChatColor.RED + "Could not create " + uuid + ".yml!");
            }
        }

        config = YamlConfiguration.loadConfiguration(file);

        this.player = player;
        this.uuid = uuid;
        this.plainUUID = player.getUniqueId();

        if (creating) {
            if (player != null) {
                config.set("username", player.getName());
                config.set("uuid", player.getUniqueId().toString());
                config.set("ip", player.getAddress().getAddress().getHostAddress());
            }

            TimeZone.setDefault(TimeZone.getTimeZone("EST"));

            config.set("firstjoined", new Date().getTime());
            config.set("lastlogin", new Date().getTime());
            config.set("lastlogoff", -1l);
            config.set("rank", Ranks.USER.name());
            config.set("staffchat", false);

            config.set("muted.status", false);
            config.set("muted.reason", "NOT_MUTED");
            config.set("muted.time", -1);

            saveFile();
        }
    }

    public boolean isNew() {
        return creating;
    }

    public Player getPlayer() {
        return player;
    }

    public String getUUID() {
        return uuid;
    }

    public UUID getPlainUUID() { return plainUUID; }

    public FileConfiguration getFile() {
        return config;
    }

    public void saveFile() {
        try {
            config.save(file);
        } catch (Exception e) {
            plugin.getLogger().severe(ChatColor.RED + "Could not save " + file.getName() + "!");
        }
    }

    public void reloadFile() {
        config = YamlConfiguration.loadConfiguration(file);
    }

    public void setRank(Ranks rank) {
        config.set("rank", rank.name());
        saveFile();

        if (player != null) {
            //Viper.getPermissionUtils().removeDefaultPermissions(player);
            //Viper.getPermissionUtils().addDefaultPermission(player);
        }
    }

    public Ranks getRank() {
        return Ranks.valueOf(config.getString("rank", "USER"));
    }

    public void mute(String reason, Date unmute) {
        config.set("muted.status", true);
        config.set("muted.reason", reason);

        if (unmute == null) {
            config.set("muted.time", -1);
        } else {
            config.set("muted.time", unmute.getTime());
        }

        saveFile();
    }

    public void unmute() {
        config.set("muted.status", false);
        config.set("muted.reason", "NOT_MUTED");
        config.set("muted.time", -1);
        saveFile();
    }

    public boolean isMuted() {
        return config.getBoolean("muted.status", false);
    }

    public String getMutedReason() {
        if (!isMuted()) {
            return "NOT_MUTED";
        }

        return config.getString("muted.reason", "NOT_MUTED");
    }

    public long getUnmuteTime() {
        if (!isMuted()) {
            return -1;
        }

        return config.getLong("muted.time", -1);
    }
}
