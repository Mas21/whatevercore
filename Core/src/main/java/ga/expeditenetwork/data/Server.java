package ga.expeditenetwork.data;

import ga.expeditenetwork.Core;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public class Server {
    private static Server instance = new Server();
    public static Server getInstance() { return instance; }

    Core plugin = Core.getInstance();

    private boolean creating = false;

    private FileConfiguration data;
    private File dFile;

    public void setup() {
        if(!plugin.getDataFolder().exists()) {
            plugin.getDataFolder().mkdir();
            creating = true;
        }
        dFile = new File(plugin.getDataFolder(), "data.yml");
        if(!dFile.exists()) {
            try {
                dFile.createNewFile();
                creating = true;
            } catch (Exception e) {
                Bukkit.getServer().getLogger().severe("Could not create data.yml!");
            }
        }
        data = YamlConfiguration.loadConfiguration(dFile);

        if(creating) {
            data.addDefault("chat.muted", false);
            data.addDefault("chat.slew", false);
            data.addDefault("commands.enabled", true);
            saveData();
        }
    }

    public FileConfiguration getData() {
        return data;
    }

    public void saveData() {
        try {
            data.save(dFile);
        } catch (Exception e) {
            Bukkit.getServer().getLogger().severe("Could not save data.yml!");
        }
    }
}
