package ga.expeditenetwork.utils;

import ga.expeditenetwork.Core;
import ga.expeditenetwork.data.Ranks;
import ga.expeditenetwork.data.Users;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;

public class ScoreboardUtils {
    private Team team;
    private Scoreboard scoreboard;
    private static Core plugin = Core.getInstance();

    public void setScoreboard(Player player) {
        Users user = Users.get(player);
        Ranks rank = user.getRank();
        String rankName;

        this.scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();

        Objective objective = this.scoreboard.registerNewObjective("ExpediteNetwork", "dummy");
        objective.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&5&lExpedite &f&lNetwork"));
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);

        this.team = this.scoreboard.registerNewTeam("Users");

        Score blank = objective.getScore(" ");
        Score s2 = objective.getScore(ChatColor.translateAlternateColorCodes('&', "&8&m+---------------+"));
        Score s3 = objective.getScore(ChatColor.translateAlternateColorCodes('&', "&8&m+---------------+"));

        Score online = objective.getScore(ChatColor.translateAlternateColorCodes('&', "&7Online: &6" + Bukkit.getServer().getOnlinePlayers().size() + "&8/&c" + Bukkit.getServer().getMaxPlayers()));

        Score ip = objective.getScore(ChatColor.translateAlternateColorCodes('&', "&7expeditenetwork.ga"));
        if(rank == Ranks.FOUNDER) {
            rankName = ChatColor.YELLOW + NameUtils.fixString(rank.name(), false);
        } else if(rank == Ranks.ADMINISTRATOR) {
            rankName = ChatColor.DARK_RED + NameUtils.fixString(rank.name(), false);
        } else if(rank == Ranks.DEVELOPER) {
            rankName = ChatColor.BLUE + NameUtils.fixString(rank.name(), false);
        } else if(rank == Ranks.MODERATOR) {
            rankName = ChatColor.DARK_GREEN + NameUtils.fixString(rank.name(), false);
        } else if(rank == Ranks.TRIAL_MOD) {
            rankName = ChatColor.GREEN + "Trial Mod";
        } else if(rank == Ranks.BUILDER) {
            rankName = ChatColor.LIGHT_PURPLE + NameUtils.fixString(rank.name(), false);
        } else {
            rankName = ChatColor.GRAY + NameUtils.fixString(rank.name(), false);
        }
        Score userRank = objective.getScore(ChatColor.translateAlternateColorCodes('&', "&7Rank: &6" + rankName));

        blank.setScore(5);
        online.setScore(4);
        s3.setScore(3);
        userRank.setScore(2);
        s2.setScore(1);
        ip.setScore(0);
    }

    public void refreshScoreboard(final Player player) {
        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
            @Override
            public void run() {
                ScoreboardUtils.this.setScoreboard(player);
                player.setScoreboard(ScoreboardUtils.this.scoreboard);
            }
        }, 20L, 20L);
    }
}
