package ga.expeditenetwork.utils;

import ga.expeditenetwork.Core;
import ga.expeditenetwork.data.Ranks;
import ga.expeditenetwork.data.Users;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;

import java.io.File;

public class PermissionUtils {

    private static Core plugin = Core.getInstance();

    //TODO: Fix permission to be more efficient.
    public void addDefaultPermission(Player player) {
        File folder = new File(plugin.getDataFolder() + File.separator + "users" + File.separator);
        boolean found = false;

        if(folder.exists()) {
            for(File file : folder.listFiles()) {
                if(file.getName().substring(0, file.getName().length() - 4).equals(player.getUniqueId().toString())) {
                    found = true;
                    break;
                }
            }
        }

        if(!found) {
            return;
        }

        if(plugin.PERMISSIONS.get(player.getUniqueId().toString()) == null) {
            plugin.PERMISSIONS.put(player.getUniqueId().toString(), player.addAttachment(plugin));
        }

        PermissionAttachment permissionAttachment = plugin.PERMISSIONS.get(player.getUniqueId().toString());

        Users user = Users.get(player);
        Ranks ranks = user.getRank();

        if(ranks == Ranks.USER) {
            player.setOp(false);
            return;
        }

        if(ranks == Ranks.FOUNDER ||
                ranks == Ranks.DEVELOPER) {
            player.setOp(true);
            permissionAttachment.setPermission("'*'", false);
            return;
        }

        if(ranks == Ranks.BUILDER) {
            permissionAttachment.setPermission("essentials.gamemode", true);
            permissionAttachment.setPermission("worldedit.*", true);
        } else if(ranks == Ranks.TRIAL_MOD ||
                ranks == Ranks.MODERATOR ||
                ranks == Ranks.ADMINISTRATOR) {
            permissionAttachment.setPermission("essentials.kick", true);
            permissionAttachment.setPermission("essentials.mute", true);
            if(ranks == Ranks.MODERATOR ||
                    ranks == Ranks.ADMINISTRATOR) {
                permissionAttachment.setPermission("essentials.tempban", true);
                permissionAttachment.setPermission("essentials.chat.manage", true);
                permissionAttachment.setPermission("essentials.bypass", true);
                permissionAttachment.setPermission("essentials.alert", true);
                if(ranks == Ranks.ADMINISTRATOR) {
                    permissionAttachment.setPermission("essentials.ban", true);
                    permissionAttachment.setPermission("essentials.banip", true);
                }
            }
        }
    }

    public void removeDefaultPermissions(Player player) {
        if(plugin.PERMISSIONS.get(player.getUniqueId().toString()) == null) {
            return;
        }

        try {
            player.removeAttachment(plugin.PERMISSIONS.get(player.getUniqueId().toString()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        plugin.PERMISSIONS.remove(player.getUniqueId().toString());
    }
}
