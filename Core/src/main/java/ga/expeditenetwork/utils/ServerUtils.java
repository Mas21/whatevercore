package ga.expeditenetwork.utils;

import ga.expeditenetwork.Core;
import org.bukkit.BanList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ServerUtils {

    Date date = new Date();
    SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd");
    SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");

    BanList nameBan = Bukkit.getBanList(BanList.Type.NAME);
    BanList ipBan = Bukkit.getBanList(BanList.Type.IP);

    Core plugin = Core.getInstance();

    public void banPlayer(CommandSender sender, Player target, String reason) {
        nameBan.addBan(target.getName(), reason, null, sender.getName());
        if(target.hasPermission("core.bypass")) {
            plugin.getMessage().sendMessage(sender, "&cError 504: &eCannot ban player.");
            return;
        }
        target.kickPlayer(ChatColor.translateAlternateColorCodes('&', "&cYou have been &4suspended &cfrom the &5Expedite &fNetwork. \n \n"
                + "&7Reason: &f" + reason + "\n"
                + "&7Suspended By: &b" + sender.getName() + "\n"
                + "&7Date: &a" + dateFormat.format(date) + "\n \n"
                + "&cIf you would like to appeal, visit our website at &a&o&nwww.(yourwebsitehere).com"));
        plugin.getMessage().systemAlert("&6" + target.getName() + " &7has been &csuspended&7.");
    }

    public void banPlayer(CommandSender sender, String target, String reason) {
        nameBan.addBan(target, reason, null, sender.getName());
        plugin.getMessage().systemAlert("&6" + target + " &7has been &csuspended&7.");
    }

    public void banIP(CommandSender sender, String ip, String reason) {
        ipBan.addBan(ip, reason, null, sender.getName());

        for(Player online : Bukkit.getServer().getOnlinePlayers()) {
            if(online.getAddress().getAddress().getHostAddress().equals(ip)) {
                online.kickPlayer(ChatColor.translateAlternateColorCodes('&', "&cYou have been &4suspended &cfrom the &5Expedite &fNetwork. \n \n"
                        + "&7Reason: &f" + reason + "\n"
                        + "&7Suspended By: &b" + sender.getName() + "\n"
                        + "&7Date: &a" + dateFormat.format(date) + "\n \n"));
            }
        }
        plugin.getMessage().systemAlert("&7An &6IP &7has been &csuspended&7.");
    }

    public void tempbanPlayer(CommandSender sender, Player target, Date time, String reason) {
        nameBan.addBan(target.getName(), reason, time, sender.getName());
        if(target.hasPermission("core.bypass")) {
            plugin.getMessage().sendMessage(sender, "&cError 504: &eCannot kick player.");
            return;
        }
        target.kickPlayer(ChatColor.translateAlternateColorCodes('&', "&cYou have been &4temporarily suspended &cfrom the &5Expedite &fNetwork. \n \n"
                + "&7Reason: &f" + reason + "\n"
                + "&7Suspended By: &b" + sender.getName() + "\n"
                + "&7Expiration: &a" + time));
        plugin.getMessage().systemAlert("&6" + target.getName() + " &7has been &ctemporarily suspended&7.");
    }

    public void tempbanPlayer(CommandSender sender, String target, Date time, String reason) {
        nameBan.addBan(target, reason, time, sender.getName());
        plugin.getMessage().systemAlert("&6" + target + " &7has been &ctemporarily suspended&7.");
    }

    public void kickPlayer(CommandSender sender, Player target, String reason) {
        if(target.hasPermission("core.bypass")) {
            plugin.getMessage().sendMessage(sender, "&cError 404: &ePlayer cannot be found.");
            return;
        }
        target.kickPlayer(ChatColor.translateAlternateColorCodes('&', "&cYou have been &4kicked &cfrom the &5Expedite &fNetwork. \n \n"
                + "&7Reason: &f" + reason + "\n"
                + "&7Kicked By: &b" + sender.getName()));
        plugin.getMessage().systemAlert("&6" + target.getName() + " &7has been &ckicked&7.");
    }

    public void unbanPlayer(CommandSender sender, String target) {
        if(!nameBan.isBanned(target)) {
            plugin.getMessage().sendMessage(sender, "&eError 404: &cPlayer not found in database.");
            return;
        }
        nameBan.pardon(target);
        plugin.getMessage().staffMessage(ChatColor.GOLD + target + " &7has been unbanned.");
    }

    public void unbanIP(CommandSender sender, String ip) {
        if(!ipBan.isBanned(ip)) {
            plugin.getMessage().sendMessage(sender, "&eError 404: &cIP not found in database.");
            return;
        }
        ipBan.pardon(ip);
        plugin.getMessage().staffMessage("&7An &6IP &7has been unbanned.");
    }
}
