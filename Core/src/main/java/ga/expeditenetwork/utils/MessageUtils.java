package ga.expeditenetwork.utils;

import ga.expeditenetwork.data.Ranks;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MessageUtils {

    private String SYSTEM_PREFIX = ChatColor.translateAlternateColorCodes('&', "&6System &8» &7");


    public void sendMessage(CommandSender player, String text) {
        player.sendMessage(ChatColor.translateAlternateColorCodes('&', text));
    }

    public void staffMessage(String text) {
        for(Player online : Bukkit.getServer().getOnlinePlayers()) {
            if(online.hasPermission("core.alert")) {
                sendMessage(online, text);
            }
        }
    }

    public void systemAlert(String text) {
        Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', SYSTEM_PREFIX + text));
    }

    public void rankUpdate(Player target, Ranks rank) {
        sendMessage(target, "&7Your rank has been updated to &6" + NameUtils.fixString(rank.name(), false));
        staffMessage("&b" + target.getName() + "&7's rank has been updated to &6" + NameUtils.fixString(rank.name(), false));
    }

    public void invalidUsage(CommandSender sender, String usage) {
        sendMessage(sender, "&cIncorrect Usage! Usage: &7" + usage);
    }

    public void noPermission(CommandSender sender) {
        sendMessage(sender, "&cYou don't have permission to use this command!");
    }

    public void sendConsoleNoUse() {
        Bukkit.getConsoleSender().sendMessage("You must be a player to execute this command!");
    }

    public void chat(String message) {
        Bukkit.broadcastMessage(message);
    }
}
