package ga.expeditenetwork;

import ga.expeditenetwork.commands.*;
import ga.expeditenetwork.listeners.PlayerJoin;
import ga.expeditenetwork.listeners.PlayerLogin;
import ga.expeditenetwork.listeners.PlayerQuit;
import ga.expeditenetwork.utils.MessageUtils;
import ga.expeditenetwork.utils.PermissionUtils;
import ga.expeditenetwork.utils.ScoreboardUtils;
import ga.expeditenetwork.utils.ServerUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;

//TODO: Database support (Global: bans, ranks, mutes, etc.)
public class Core extends JavaPlugin {
    private static final Core instance = new Core();
    public static final Core getInstance() { return instance; }

    public HashMap<String, Integer> SLEW_COOLDOWN = new HashMap<>();
    public HashMap<String, BukkitRunnable> SLEW_TASK = new HashMap<>();
    public HashMap<String, PermissionAttachment> PERMISSIONS = new HashMap<>();
    public HashMap<CommandSender, CommandSender> MESSAGE = new HashMap<>();

    private static MessageUtils messageUtils;
    private static ScoreboardUtils scoreboardUtils;
    private static ServerUtils serverUtils;
    private static PermissionUtils permissionUtils;

    @Override
    public void onEnable() {
        registerCommands();
        registerListeners();
        registerUtils();
    }

    @Override
    public void onDisable() {
        messageUtils = null;
        scoreboardUtils = null;
        serverUtils = null;
        permissionUtils = null;
    }

    public void registerCommands() {
        getCommand("ban").setExecutor(new BanCommand());
        getCommand("banip").setExecutor(new BanIPCommand());
        getCommand("chat").setExecutor(new ChatCommand());
        getCommand("fly").setExecutor(new FlyCommand());
        getCommand("gamemode").setExecutor(new GamemodeCommand());
        getCommand("kick").setExecutor(new KickCommand());
        getCommand("mute").setExecutor(new MuteCommand());
        getCommand("setrank").setExecutor(new SetRankCommand());
        getCommand("tempban").setExecutor(new TempbanCommand());
        getCommand("unban").setExecutor(new UnbanCommand());
        getCommand("unbanip").setExecutor(new UnbanIPCommand());
        getCommand("vanish").setExecutor(new VanishCommand());
    }

    public void registerListeners() {
        PluginManager pluginManager = Bukkit.getServer().getPluginManager();
        pluginManager.registerEvents(new PlayerJoin(), this);
        pluginManager.registerEvents(new PlayerLogin(), this);
        pluginManager.registerEvents(new PlayerQuit(), this);
    }

    public void registerUtils() {
        messageUtils = new MessageUtils();
        scoreboardUtils = new ScoreboardUtils();
        serverUtils = new ServerUtils();
        permissionUtils = new PermissionUtils();
    }

    public MessageUtils getMessage() {
        return messageUtils;
    }
    public ScoreboardUtils getScoreboards() {
        return scoreboardUtils;
    }
    public ServerUtils getServerUtils() { return serverUtils; }
    public static PermissionUtils getPermissionUtils() { return permissionUtils; }
}
