package ga.expeditenetwork.commands;

import ga.expeditenetwork.Core;
import ga.expeditenetwork.data.Users;
import ga.expeditenetwork.utils.DateUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Date;
import java.util.TimeZone;

public class MuteCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Core plugin = Core.getInstance();
        if(!sender.hasPermission("expedite.mute")) {
            plugin.getMessage().noPermission(sender);
            return false;
        }

        if(args.length == 0) {
            plugin.getMessage().invalidUsage(sender, "/mute <player> [time] [reason]");
            return false;
        }
        Player target = Bukkit.getServer().getPlayer(args[0]);
        Users users = Users.get(target);

        if(target == null) {
            plugin.getMessage().sendMessage(sender, "&eError 404: &cPlayer not found.");
            return false;
        }

        if(users.isMuted()) {
            users.unmute();
            plugin.getMessage().systemAlert(ChatColor.GOLD + target.getName() + " &7has been un-muted.");
            plugin.getMessage().sendMessage(target, "&7You are no longer muted.");
            return true;
        }

        if(args.length < 3) {
            plugin.getMessage().invalidUsage(sender, "/mute <player> <time> <reason>");
            return false;
        }

        StringBuilder message = new StringBuilder("");
        for(int i = 2; i < args.length; i++) {
            message.append(args[i]).append(" ");
        }
        String reason = message.toString().trim();

        plugin.getMessage().systemAlert(ChatColor.GOLD + target.getName() + " &7has been " + (args[1].equals("-") ? "muted" : "temp-muted") + ".");
        plugin.getMessage().sendMessage(target, "&7You have been muted for &6" + reason);

        TimeZone.setDefault(TimeZone.getTimeZone("EST"));
        long time = DateUtils.parseDateDiff(args[1], true);

        users.mute(reason, (time <= 0 ? null : new Date(time)));
        return true;
    }
}
