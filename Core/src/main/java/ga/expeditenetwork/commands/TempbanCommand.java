package ga.expeditenetwork.commands;

import ga.expeditenetwork.Core;
import ga.expeditenetwork.utils.DateUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Date;

public class TempbanCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Core plugin = Core.getInstance();
        if(!sender.hasPermission("expedite.tempban")) {
            plugin.getMessage().noPermission(sender);
            return false;
        }
        if(args.length < 3) {
            plugin.getMessage().invalidUsage(sender, "/tempban <player> <time> <reason>");
            return false;
        }
        Player target = Bukkit.getServer().getPlayer(args[0]);

        long time = DateUtils.parseDateDiff(args[1], true);
        Date date = new Date(time);

        StringBuilder reason = new StringBuilder();
        for(int i = 2; i < args.length; i++) {
            reason.append(args[i]).append(" ");
        }
        String message = reason.toString().trim();

        if(target == null) {
            plugin.getServerUtils().tempbanPlayer(sender, args[0], date, message);
            return true;
        }
        plugin.getServerUtils().tempbanPlayer(sender, target, date, message);
        return true;
    }
}
