package ga.expeditenetwork.commands;

import ga.expeditenetwork.Core;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class VanishCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Core plugin = Core.getInstance();
        if(!(sender instanceof Player)) {
            plugin.getMessage().sendConsoleNoUse();
            return true;
        }
        Player player = (Player) sender;

        if(!player.hasPermission("expedite.vanish")) {
            plugin.getMessage().noPermission(player);
            return true;
        }

        for(Player online : Bukkit.getServer().getOnlinePlayers()) {
            if(online.canSee(player)) {
                online.hidePlayer(player);
                plugin.getMessage().sendMessage(player, "&7You have &aenabled &7vanish.");
                return true;
            } else {
                online.showPlayer(player);
                plugin.getMessage().sendMessage(player, "&7You have &cdisabled &7vanish.");
            }
        }
        return true;
    }
}
