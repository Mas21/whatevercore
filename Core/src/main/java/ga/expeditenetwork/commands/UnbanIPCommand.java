package ga.expeditenetwork.commands;

import ga.expeditenetwork.Core;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class UnbanIPCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Core plugin = Core.getInstance();
        if(!sender.hasPermission("expedite.unbanip")) {
            plugin.getMessage().noPermission(sender);
            return true;
        }
        if(args.length < 2) {
            plugin.getMessage().invalidUsage(sender, "/unbanip <ip>");
            return true;
        }
        String ip = args[0];
        plugin.getServerUtils().unbanPlayer(sender, ip);
        return true;
    }
}
