package ga.expeditenetwork.commands;

import ga.expeditenetwork.Core;
import ga.expeditenetwork.data.Server;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ChatCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Core plugin = Core.getInstance();
        if(!sender.hasPermission("essentials.chat.manage")) {
            plugin.getMessage().noPermission(sender);
            return false;
        }
        if(args.length == 0) {
            plugin.getMessage().invalidUsage(sender, "/chat <clear|mute|slew>");
            return false;
        }
        Server server = Server.getInstance();
        int i;
        if(args[0].equalsIgnoreCase("mute")) {
            if(!server.getData().getBoolean("chat.muted")) {
                server.getData().set("chat.muted", true);
                server.saveData();
                plugin.getMessage().systemAlert("&7The chat has been muted.");
                return true;
            } else if(server.getData().getBoolean("chat.muted")){
                server.getData().set("chat.muted", false);
                server.saveData();
                plugin.getMessage().systemAlert("The chat has been un-muted.");
            } else {
                plugin.getMessage().sendMessage(sender, "&eError 404: &cPath not found.");
            }
        } else if(args[0].equalsIgnoreCase("clear")) {
            for(i = 0; i < 200; i++) {
                Bukkit.broadcastMessage(" ");
            }
            plugin.getMessage().systemAlert("&7The chat has been cleared.");
        } else if(args[0].equalsIgnoreCase("slew")) {
            if(!server.getData().getBoolean("chat.slew")) {
                server.getData().set("chat.slew", true);
                server.saveData();
                plugin.getMessage().systemAlert("&7The chat has been slowed.");
                plugin.getMessage().systemAlert("&7You can chat once every &c10 &7seconds.");
                return true;
            } else if(server.getData().getBoolean("chat.slew")){
                server.getData().set("chat.slew", false);
                server.saveData();
                for(Player online : Bukkit.getServer().getOnlinePlayers()) {
                    plugin.SLEW_COOLDOWN.remove(online.getUniqueId().toString());
                    plugin.SLEW_TASK.remove(online.getUniqueId().toString());
                }
                plugin.getMessage().systemAlert("That chat has been set back to normal.");
            } else {
                plugin.getMessage().sendMessage(sender, "&eError 404: &cPath not found.");
            }
        }
        return true;
    }
}
