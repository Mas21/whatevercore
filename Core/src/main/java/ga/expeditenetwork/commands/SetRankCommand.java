package ga.expeditenetwork.commands;

import ga.expeditenetwork.Core;
import ga.expeditenetwork.data.Ranks;
import ga.expeditenetwork.data.Users;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

//TODO: Open GUI for ranks...
public class SetRankCommand implements CommandExecutor, TabCompleter {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Core plugin = Core.getInstance();
        if(!sender.hasPermission("expedite.setrank")) {
            plugin.getMessage().noPermission(sender);
            return true;
        }
        if(args.length < 2) {
            plugin.getMessage().invalidUsage(sender, "/setrank <player> <rank>");
            return true;
        }
        Player target = Bukkit.getServer().getPlayer(args[0]);
        Ranks ranks;

        if (target == null) {
            plugin.getMessage().sendMessage(sender, "&eError 404: &cPlayer not found.");
            return true;
        }

        try {
            ranks = Ranks.valueOf(args[1].toUpperCase());
        } catch (Exception e) {
            plugin.getMessage().sendMessage(sender, "&eError 404: &cRank not found.");
            return true;
        }

        Core.getPermissionUtils().removeDefaultPermissions(target);
        Core.getPermissionUtils().addDefaultPermission(target);
        plugin.getMessage().rankUpdate(target, ranks);
        Users.get(target).setRank(ranks);
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        if(!sender.hasPermission("expedite.setrank")) {
            return null;
        }

        ArrayList<String> toReturn = new ArrayList<>();

        if(args.length == 1) {
            if(args[0].equalsIgnoreCase("")) {
                for(Player online : Bukkit.getServer().getOnlinePlayers()) {
                    toReturn.add(online.getName());
                }
            } else {
                for(Player online : Bukkit.getServer().getOnlinePlayers()) {
                    if(online.getName().toLowerCase().startsWith(args[0].toLowerCase())) {
                        toReturn.add(online.getName());
                    }
                }
            }
        }

        if(args.length == 2) {
            if(args[1].equalsIgnoreCase("")) {
                for(Ranks rankData : Ranks.values()) {
                    toReturn.add(rankData.name().toLowerCase());
                }
            } else {
                for(Ranks rankData : Ranks.values()) {
                    if(rankData.name().toLowerCase().startsWith(args[1].toLowerCase())) {
                        toReturn.add(rankData.name().toLowerCase());
                    }
                }
            }
        }
        return toReturn;
    }
}
