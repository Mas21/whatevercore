package ga.expeditenetwork.commands;

import ga.expeditenetwork.Core;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class BanIPCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Core plugin = Core.getInstance();
        if(!sender.hasPermission("expedite.banip")) {
            plugin.getMessage().noPermission(sender);
            return true;
        }
        if(args.length < 2) {
            plugin.getMessage().invalidUsage(sender, "/banip <ip> <reason>");
            return true;
        }
        String ip = args[0];

        StringBuilder reason = new StringBuilder();
        for(int i = 1; i < args.length; i++) {
            reason.append(args[i]).append(" ");
        }
        String message = reason.toString().trim();

        plugin.getServerUtils().banIP(sender, ip, message);
        return true;
    }
}
