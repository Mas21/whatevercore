package ga.expeditenetwork.commands;

import ga.expeditenetwork.Core;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GamemodeCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Core plugin = Core.getInstance();
        if (!(sender instanceof Player)) {
            plugin.getMessage().sendConsoleNoUse();
            return false;
        }
        Player player = (Player) sender;

        if(!player.hasPermission("expedite.gamemode")) {
            plugin.getMessage().noPermission(player);
            return true;
        }

        if (args.length == 0) {
            plugin.getMessage().invalidUsage(player, "/gamemode <mode> [player]");
            return false;
        }

        GameMode mode = null;

        try {
            mode = GameMode.getByValue(Integer.parseInt(args[0]));
        } catch (Exception e) {
            for (GameMode modes : GameMode.values()) {
                if (modes.name().startsWith(args[0].toUpperCase())) {
                    mode = modes;
                    break;
                }
            }
        }

        if (mode == null) {
            plugin.getMessage().sendMessage(player, ChatColor.RED + args[0] + " is not a vaild gamemode.");
            return true;
        }

        if (args.length == 1) {
            plugin.getMessage().sendMessage(player, "&7You are now in &6" + mode.name().toLowerCase() + " &7mode.");
            player.setGameMode(mode);
            return true;
        }
        if (!sender.hasPermission("expedite.gamemode.other")) {
            plugin.getMessage().noPermission(player);
            return true;
        }

        Player target = Bukkit.getServer().getPlayer(args[1]);

        if (target == null) {
            plugin.getMessage().sendMessage(sender, "&eError 404: &cPlayer not found.");
            return true;
        }

        plugin.getMessage().sendMessage(player, "&7You have changed &a" + target.getName() + "'s &7gamemode to &6" + mode.name().toLowerCase() + " &7mode.");
        plugin.getMessage().sendMessage(target, "&7You are now in &6" + mode.name().toLowerCase() + " &7mode.");
        target.setGameMode(mode);
        return true;
    }
}
