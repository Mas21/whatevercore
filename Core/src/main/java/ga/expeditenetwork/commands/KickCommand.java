package ga.expeditenetwork.commands;

import ga.expeditenetwork.Core;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class KickCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Core plugin = Core.getInstance();
        if(!sender.hasPermission("expedite.kick")) {
            plugin.getMessage().noPermission(sender);
            return true;
        }
        if(args.length < 2) {
            plugin.getMessage().invalidUsage(sender, "/kick <player> <reason>");
            return true;
        }
        Player target = Bukkit.getServer().getPlayer(args[0]);

        StringBuilder reason = new StringBuilder();
        for(int i = 1; i < args.length; i++) {
            reason.append(args[i]).append(" ");
        }
        String message = reason.toString().trim();

        if(target == null) {
            plugin.getMessage().sendMessage(sender, "&eError 404: &cPlayer not found.");
            return true;
        }
        plugin.getServerUtils().kickPlayer(sender, target, message);
        return true;
    }
}
