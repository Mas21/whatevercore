package ga.expeditenetwork.commands;

import ga.expeditenetwork.Core;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class FlyCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Core plugin = Core.getInstance();
        if(!(sender instanceof Player)) {
            plugin.getMessage().sendConsoleNoUse();
            return true;
        }
        Player player = (Player) sender;
        if(!player.hasPermission("expedite.fly")) {
            plugin.getMessage().noPermission(player);
            return true;
        }

        if(!player.getAllowFlight()) {
            player.setAllowFlight(true);
            plugin.getMessage().sendMessage(player, "&7You have &aenabled &7flight.");
        } else {
            player.setAllowFlight(false);
            player.setFlying(false);
            plugin.getMessage().sendMessage(player, "&7You have &cenabled &7flight.");
        }
        return true;
    }
}
